![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)
![KimDo7](https://img.shields.io/badge/KimDo7-red.svg)

### What is this repository for?

- Quick summary: This is a lightweight library that has the same funcationality of lodash. It is suited for lightweight webcomponenet such as Lit-Element/Polymer 3
- Version `1.0.7`

### Installation

`npm i get-es6 --save`

---

Then

```js
import { get } from "get-es6";
```

### Sample 1

```js
const object = { a: [{ b: { c: 3 } }] };

get(object, "a[0].b.c"); // = 3
get(object, ["a", "0", "b", "c"]); // = 3
get(object, "a.b.c", "default"); // = "default"
```

### Sample 2

```js
const landmark = {
  name: "Golden Gate Bridge",
  location: {
    type: "Feature",
    properties: {
      city: "San Francisco",
      state: "California",
    },
    geometry: {
      type: "Point",
      coordinates: [-122.4804438, 37.8199328],
    },
  },
};

get(landmark, "location.geometry.type"); // = "Point"
get(landmark, "location.geometry.coordinates.0" // = -122.4804438
get(landmark, "location.geometry.coordinates[1]" // = 37.8199328

delete landmark.location;
get(landmark, "location.geometry.type") === undefined // = undefined
```

### Who do I talk to?

- Repo owner or admin: `Kim Do`
- Linkedin: [@kimdo7](https://www.linkedin.com/in/kimdo7/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
