const _getHelper = (value, fragments, fallback) => {
  if (value === undefined) {
    return fallback;
  } else if (fragments.length === 0) {
    return value;
  }

  return _getHelper(value[fragments.shift()], fragments, fallback);
};

export const get = (obj, path, fallback) => {
  const filter = (path) => path.replace(/\[/gi, ".").replace(/\]/gi, "");
  const fragments = (Array.isArray(path) && path) || filter(path).split(".");

  return _getHelper(obj, fragments, fallback);
};